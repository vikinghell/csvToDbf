/**
 * 
 */
package br.com.csvToDbf;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.linuxense.javadbf.DBFDataType;
import com.linuxense.javadbf.DBFException;
import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFWriter;

import br.com.csvToDbf.model.Table;

/**
 * @author legna
 *
 */
public class Main {

	private static BufferedReader input;
	private static ArrayList<Table> list = new ArrayList<Table>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		// Display a Welcome Message
		JOptionPane.showMessageDialog(null,
				"\t\t\t Welcome to the CsvToDbf.\n" + " 	This is a free and open source software that allows you"
						+ " to import data from CSV file to a new dbf table.",
				"Welcome Message", JOptionPane.PLAIN_MESSAGE);

		readFiles();

		if (list != null)
			createDbf(list);
	}

	/**
	 * Allows the user to select a file and then create a "Table" object. This
	 * example reads a file with only two fields, but you can modify it to read as
	 * many fields as you want
	 */
	private static void readFiles() {

		JFileChooser dlg = new JFileChooser("C:\\Users\\legna\\Documents\\");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma Separated File", "csv", "csv");
		dlg.setFileFilter(filter);
		dlg.setDialogTitle("Select the File to Use");

		if (dlg.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

			try {
				String line = "";

				input = new BufferedReader(
						new InputStreamReader(new FileInputStream(dlg.getSelectedFile().getPath()), "UTF-8"));

				input.readLine();
				while ((line = input.readLine()) != null) {
					String[] rows = line.split("[|\n]");
					list.add(new Table(rows[0], rows[1])); // add the table object to an local ArrayList variable
				}

				if (list != null) {
					System.out.println("Lista criada com sucesso!");
					for (int i = 0; i < list.size(); i++) {
						System.out
								.println(list.get(i).getCode() + "\t" + "Description= " + list.get(i).getDescription());
					}
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		} else {
			JOptionPane.showMessageDialog(null, "No file selected - program terminated", "Program Terminated",
					JOptionPane.ERROR_MESSAGE);
			System.exit(-8);
		}
	}

	/**
	 * Gets a list and writes the content into an new dbf file
	 * 
	 * @param list
	 */
	public static void createDbf(ArrayList<Table> list) {
		int code;
		// let us create field definitions first
		// we will go for 3 fields

		JFileChooser fileChooser = new JFileChooser("C:\\Users\\legna\\Documents\\");

		if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			try {
				DBFField fields[] = new DBFField[2];

				fields[0] = new DBFField();
				fields[0].setName("cod_cfop");
				fields[0].setType(DBFDataType.NUMERIC);
				fields[0].setFieldLength(10);

				fields[1] = new DBFField();
				fields[1].setName("desc_cfop");
				fields[1].setType(DBFDataType.CHARACTER);
				fields[1].setFieldLength(254);

				DBFWriter writer = new DBFWriter();
				writer.setFields(fields);

				// now populate DBFWriter
				for (int i = 0; i < list.size(); i++) {

					Object rowData[] = new Object[2];

					code = Integer.parseInt(list.get(i).getCode());

					rowData[0] = code;
					rowData[1] = list.get(i).getDescription();

					writer.addRecord(rowData);
				}

				FileOutputStream fos = new FileOutputStream(fileChooser.getSelectedFile().getPath());

				writer.write(fos);

				if (fos != null)
					JOptionPane.showMessageDialog(null, "\r\n" + "Dbf table was successfully created");

				fos.close();

			} catch (IOException e) {
				e.printStackTrace();
			} catch (DBFException e) {
				e.printStackTrace();
			}
		} else {

		}
	}
}
